import java.util.Scanner;
public class AreaOfACircle {
  public static void main(String[] args) {
    double r;
    double area;
    Scanner teclado = new Scanner(System.in);

    r = teclado.nextDouble();
    area = (3.14159)*Math.pow(r, 2);

    System.out.println("Area = "+ area);
  }
}
