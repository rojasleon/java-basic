import java.util.Scanner;

public class Sum {
  public static void main(String[] args) {
    int n1, n2, SOMA;
    Scanner t = new Scanner(System.in);

    n1 = t.nextInt();
    n2 = t.nextInt();

    SOMA = n1+n2;

    System.out.println("SOMA = " + SOMA);
  }
}
